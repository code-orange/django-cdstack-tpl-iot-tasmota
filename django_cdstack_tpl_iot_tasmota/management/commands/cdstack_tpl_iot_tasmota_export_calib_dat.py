import json
from django.core.management.base import BaseCommand

from django_cdstack_models.django_cdstack_models.models import CmdbGroup, CmdbHost


class Command(BaseCommand):
    def handle(self, *args, **options):
        # iot_tasmota_groups = CmdbGroup.objects.filter(config_pkg="iot_tasmota")
        for host in CmdbHost.objects.filter(id__in=(225023, 225024, 225025, 225026)):
            with open(f"tasmota-calib-dat-{host.id}.txt", "wt") as ofile:
                calib_dat = host.cmdbvarshost_set.get(name="shelly_3em_calib_dat").value
                calib_dat_json = json.loads(calib_dat)
                ofile.write(
                    "rule3 on file#calib.dat do "
                    + json.dumps(calib_dat_json)
                    + " endon"
                )
